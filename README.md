# a fun brick
An interpreter of the brainfuck language and some of its derivatives

## Standards

### Currently supported
* [Brainfuck](http://esolangs.org/wiki/Brainfuck)
* [Ook!](http://esolangs.org/wiki/ook!)
* [Brainfuck--](http://esolangs.org/wiki/Brainfuck--)
* [Loose Circular Brainfuck (LCBF)](http://esolangs.org/wiki/Loose_Circular_Brainfuck_%28LCBF%29)
* [Brainfuck+3](https://esolangs.org/wiki/Brainfuck%2B3)
* [M×N-Dimensional Brainfuck](http://esolangs.org/wiki/M%C3%97N-Dimensional_Brainfuck)

### Plan to support
* [Brainfuck++](http://esolangs.org/wiki/Brainfuck%2B%2B)
* [*Brainfuck](http://esolangs.org/wiki/*brainfuck)
* [Brainfork](http://esolangs.org/wiki/Brainfork)

## Other features
* Code optimization
* Translation Brainfuck -> Ook!
* Translation Ook! -> Brainfuck
