CC = cc
INCLUDES = -I src -I deps
CFLAGS = -std=c99 -Wall -Wextra -Wconversion -Werror --pedantic $(INCLUDES)
LDFLAGS =
UPX_CMD =
MODE = debug
LIBC = gnu
ARCH = $(shell uname -m)
OS = $(shell uname | tr '/[:upper:]' '_[:lower:]' )

OBJECTS_FILENAMES = config.o bytecode.o vectors.o parser.o bf/parser.o bf/machine.o bfmm/parser.o bfmm/machine.o lcbf/machine.o ook/parser.o mndb/machine.o bfp3/parser.o bfp3/machine.o machine.o main.o
TARGET_FILENAME = afunbrick

ifdef MUSL
	CC = musl-gcc
	LDFLAGS += -static
	LIBC = musl
endif

ifdef RELEASE
	CFLAGS += -O9 -DNDEBUG
	LDFLAGS += -s
	UPX_CMD = upx --brute $(TARGET)
	MODE = release
else
	CFLAGS += -g -O0
endif

TARGET_DIR = target/$(ARCH)-$(OS)-$(LIBC)/$(MODE)
OBJECTS_DIR = $(TARGET_DIR)/obj
OBJECTS = $(OBJECTS_FILENAMES:%=$(OBJECTS_DIR)/%)
TARGET = $(TARGET_DIR)/$(TARGET_FILENAME)

include deps/module.mk

.PHONY: help clean test

help:
	@echo "main targets: all test clean"
	@echo "options:"
	@echo "    RELEASE=1    remove debug outputs, enable optimization and strip the executable"
	@echo "    MUSL=1       link statically against musl libc"

all: $(TARGET)

$(TARGET): $(OBJECTS)
	$(CC) $^ -o $@ $(LDFLAGS)
	$(UPX_CMD)

$(OBJECTS_DIR)/%.o: src/%.c $(DEPS)
	@mkdir -p $(dir $@)
	$(CC) $(CFLAGS) $< -c -o $@

clean:
	/bin/rm -rf target

include test/module.mk
