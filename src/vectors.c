#include <stddef.h>
#include <stdbool.h>
#include <stdio.h>

#include "vectors.h"

#include "config.h"
#include "bytecode.h"

#include <cvector/vec_def.h>

DEFINE_VECTOR(afb_bcvec, struct afb_bytecode)
DEFINE_VECTOR(sizevec, size_t)
DEFINE_VECTOR(sizevecvec, sizevec)
