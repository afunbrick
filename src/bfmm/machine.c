#include <stddef.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdint.h>
#include <sys/ioctl.h>
#include <unistd.h>

#include "config.h"
#include "vectors.h"
#include "bytecode.h"

#include "bfmm/machine.h"

static size_t get_term_width()
{
    struct winsize w;
    ioctl(STDOUT_FILENO, TIOCGWINSZ, &w);
    return w.ws_col;
}

static uint32_t get_mask(struct afb_bfmm_machine* machine)
{
    return UINT32_C(1) << machine->i;
}

void afb_bfmm_machine_init(struct afb_bfmm_machine* machine, struct afb_config* config)
{
    (void) config;
    *machine = (struct afb_bfmm_machine) { .mem = 0, .i = 0 };
}

void afb_bfmm_machine_free(struct afb_bfmm_machine* machine)
{
    (void) machine;
}

void afb_bfmm_machine_print_memory(struct afb_bfmm_machine* machine, struct afb_config* config)
{
    size_t ncol = (get_term_width() - 8) / 5;
    if (ncol == 0) {
        ncol = 1;
    } else if (ncol > 32) {
        ncol = 32;
    }
    fprintf(config->err_stream, "       ");
    for (size_t i = 0; i < ncol; i++) {
        fprintf(config->err_stream, " %3zu ", i);
    }
    fprintf(config->err_stream, " \n");
    fprintf(config->err_stream, "      +");
    for (size_t i = 0; i < ncol; i++) {
        fprintf(config->err_stream, "-----");
    }
    fprintf(config->err_stream, "+\n");
    {
        size_t i = 0;
        for (; i < 32; i++) {
            if ((i % ncol) == 0) {
                if (i != 0) {
                    fprintf(config->err_stream, "|\n");
                }
                fprintf(config->err_stream,"%5zu |", i);
            }
            if (i == machine->i) {
                fprintf(config->err_stream, "[ %u ]", (UINT32_C(1) << i) & machine->mem ? 1 : 0);
            } else {
                fprintf(config->err_stream, "  %u  ", (UINT32_C(1) << i) & machine->mem ? 1 : 0);
            }
        }
        while ((i % ncol) != 0) {
            fprintf(config->err_stream, "     ");
            i++;
        }
    }
    fprintf(config->err_stream, "|\n      +");
    for (size_t i = 0; i < ncol; i++) {
        fprintf(config->err_stream, "-----");
    }
    fprintf(config->err_stream, "+\n");
}

static bool get_value(struct afb_bfmm_machine* machine)
{
    return machine->mem & get_mask(machine) ? true : false;
}

static void inc_value(struct afb_bfmm_machine* machine)
{
    machine->mem ^= get_mask(machine);
}

static void write_value(struct afb_bfmm_machine* machine)
{
    if (get_value(machine)) {
        putchar((uint8_t) (machine->mem & 0xFF));
    } else {
        int c = getchar();
        if (c >= 0) {
            machine->mem &= 0xFFFFFF00;
            machine->mem |= (uint32_t) (uint8_t) c;
        }
    }
}

static void go_right(struct afb_bfmm_machine* machine)
{
    machine->i = (machine->i + 1) % 32;
}

static int exec_block(struct afb_bfmm_machine* machine, struct afb_bcvec* block, struct afb_config* config)
{
    int res = 0;
    for (size_t i = 0; (i < block->size) && (res >= 0); i++) {
        res |= afb_bfmm_exec(machine, afb_bcvec_getp(block, i), config);
    }
    return res;
}

int afb_bfmm_exec(struct afb_bfmm_machine* machine, struct afb_bytecode* bytecode, struct afb_config* config)
{
    int res = 0;
    if (config->debug) {
        fprintf(config->err_stream, "%s:\n", afb_bytecode_ins_desc(bytecode->ins));
    }
    switch (bytecode->ins) {
        case INS_PROGRAM:
            return exec_block(machine, &bytecode->block, config);
        case INS_WHILE:
            while (get_value(machine) && (res >= 0)) {
                res |= exec_block(machine, &bytecode->block, config);
            }
            break;
        case INS_RIGHT: go_right(machine);      break;
        case INS_INC:   inc_value(machine);     break;
        case INS_WRITE: write_value(machine);   break;
        default:
            // unsupported instruction
            res = -1;
    }
    if (config->debug) {
        afb_bfmm_machine_print_memory(machine, config);
    }
    return res;
}
