#include <stddef.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>

#include "config.h"
#include "vectors.h"
#include "bytecode.h"
#include "parser.h"
#include "bf/machine.h"
#include "bfmm/machine.h"
#include "lcbf/machine.h"
#include "mndb/machine.h"
#include "bfp3/machine.h"
#include "machine.h"


int main(int argc, char** argv)
{
    (void) argc;
    int err = 0;
    struct afb_config config;
    afb_config_init(&config);
    err |= afb_config_parse(&config, argv);
    if (err || config.help) {
        afb_config_usage(&config);
        goto end;
    }
    err |= afb_config_open_streams(&config);
    if (err) goto end;
    struct afb_bytecode program;
    afb_bytecode_init(&program);
    afb_parse(&program, &config);
    if (config.print) {
        afb_bytecode_write(&program, &config);
        fputc('\n', stdout);
    }
    struct afb_machine machine;
    afb_machine_init(&machine, &config);
    if (config.debug) {
        afb_machine_print_memory(&machine, &config);
    }
    int machine_err = afb_exec(&machine, &program, &config);
    if (machine_err < 0) {
        fprintf(config.err_stream, "machine error: %s\n", afb_machine_strerror(machine_err));
    }
    afb_machine_free(&machine);
    afb_bytecode_free(&program);
end:
    afb_config_free(&config);
    return err;
}
