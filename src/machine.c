#include <stddef.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdint.h>
#include <sys/ioctl.h>
#include <unistd.h>

#include "config.h"
#include "vectors.h"
#include "bytecode.h"
#include "bf/machine.h"
#include "bfmm/machine.h"
#include "lcbf/machine.h"
#include "mndb/machine.h"
#include "bfp3/machine.h"

#include "machine.h"

const char* afb_machine_strerror(int err)
{
    switch (err) {
        case -1:
            return "insupported instruction";
        default:
            return NULL;
    }
}

void afb_machine_init(struct afb_machine* machine, struct afb_config* config)
{
    machine->arch = config->std;
    switch (machine->arch) {
        case STD_BRAINFUCK:
        case STD_OOK:
            afb_bf_machine_init(&machine->hard.bf, config);
            break;
        case STD_BRAINFUCKMM:
            afb_bfmm_machine_init(&machine->hard.bfmm, config);
            break;
        case STD_LCBF:
            afb_lcbf_machine_init(&machine->hard.lcbf, config);
            break;
        case STD_MNDB:
            afb_mndb_machine_init(&machine->hard.mndb, config);
            break;
        case STD_BFP3:
            afb_bfp3_machine_init(&machine->hard.bfp3, config);
            break;
        default:
            break;
    }
}

void afb_machine_free(struct afb_machine* machine)
{
    switch (machine->arch) {
        case STD_BRAINFUCK:
        case STD_OOK:
            afb_bf_machine_free(&machine->hard.bf);
            break;
        case STD_BRAINFUCKMM:
            afb_bfmm_machine_free(&machine->hard.bfmm);
            break;
        case STD_LCBF:
            afb_lcbf_machine_free(&machine->hard.lcbf);
            break;
        case STD_MNDB:
            afb_mndb_machine_free(&machine->hard.mndb);
            break;
        case STD_BFP3:
            afb_bfp3_machine_free(&machine->hard.bfp3);
            break;
        default:
            break;
    }
}

void afb_machine_print_memory(struct afb_machine* machine, struct afb_config* config)
{
    switch (machine->arch) {
        case STD_BRAINFUCK:
        case STD_OOK:
            afb_bf_machine_print_memory(&machine->hard.bf, config);
            break;
        case STD_BRAINFUCKMM:
            afb_bfmm_machine_print_memory(&machine->hard.bfmm, config);
            break;
        case STD_LCBF:
            afb_lcbf_machine_print_memory(&machine->hard.lcbf, config);
            break;
        case STD_MNDB:
            afb_mndb_machine_print_memory(&machine->hard.mndb, config);
            break;
        case STD_BFP3:
            afb_bfp3_machine_print_memory(&machine->hard.bfp3, config);
            break;
        default:
            break;
    }
}

int afb_exec(struct afb_machine* machine, struct afb_bytecode* bytecode, struct afb_config* config)
{
    switch (machine->arch) {
        case STD_BRAINFUCK:
        case STD_OOK:
            return afb_bf_exec(&machine->hard.bf, bytecode, config);
        case STD_BRAINFUCKMM:
            return afb_bfmm_exec(&machine->hard.bfmm, bytecode, config);
        case STD_LCBF:
            return afb_lcbf_exec(&machine->hard.lcbf, bytecode, config);
        case STD_MNDB:
            return afb_mndb_exec(&machine->hard.mndb, bytecode, config);
        case STD_BFP3:
            return afb_bfp3_exec(&machine->hard.bfp3, bytecode, config);
        default:
            return -2;
    }
}
