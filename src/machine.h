struct afb_machine {
    enum afb_standard arch;
    union {
        struct afb_bf_machine bf;
        struct afb_bfmm_machine bfmm;
        struct afb_lcbf_machine lcbf;
        struct afb_mndb_machine mndb;
        struct afb_bfp3_machine bfp3;
    } hard;
};

void afb_machine_init(struct afb_machine* machine, struct afb_config* config);

void afb_machine_free(struct afb_machine* machine);

void afb_machine_print_memory(struct afb_machine* machine, struct afb_config* config);

int afb_exec(struct afb_machine* machine, struct afb_bytecode* bytecode, struct afb_config* config);

const char* afb_machine_strerror(int err);
