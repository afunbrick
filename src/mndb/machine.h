enum afb_mndb_direction {
    MNDB_RIGHT = 0,
    MNDB_DOWN = 1,
    MNDB_LEFT = 2,
    MNDB_UP = 3,
};

struct afb_mndb_machine {
    size_t** mem;
    size_t memsize;
    enum afb_mndb_direction dir;
    size_t i;
    size_t j;
    size_t mask;
};

void afb_mndb_machine_init(struct afb_mndb_machine* machine, struct afb_config* config);

int afb_mndb_exec(struct afb_mndb_machine* machine, struct afb_bytecode* bytecode, struct afb_config* config);

void afb_mndb_machine_print_memory(struct afb_mndb_machine* machine, struct afb_config* config);

void afb_mndb_machine_free(struct afb_mndb_machine* machine);
