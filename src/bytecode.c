#include <stdio.h>

#include "vectors.h"
#include "config.h"

#include "bytecode.h"

static const char* const ins_descs[] = {
        "program",
        "left",
        "right",
        "inc",
        "dec",
        "write",
        "read",
        "while",
        "up",
        "down",
        "exit",
};

const char* afb_bytecode_ins_desc(enum afb_ins ins) {
    return ins_descs[ins];
}

void afb_bytecode_init(struct afb_bytecode* program)
{
    program->ins = INS_PROGRAM;
    program->block = afb_bcvec_create(0);
}

void afb_bytecode_free(struct afb_bytecode* bytecode)
{
    switch (bytecode->ins) {
        case INS_PROGRAM:
        case INS_WHILE:
            for (size_t i = 0; i < bytecode->block.size; i++) {
                afb_bytecode_free(afb_bcvec_getp(&bytecode->block, i));
            }   
            afb_bcvec_free(&bytecode->block);
            break;
        default:
            break;
    }       
}   

void afb_bytecode_write(struct afb_bytecode* bytecode, struct afb_config* config)
{
    for (size_t i = 0; i < bytecode->block.size; i++) {
        struct afb_bytecode* bc = afb_bcvec_getp(&bytecode->block, i);
        switch (bc->ins) {
            case INS_WHILE:
                fputc('[', config->err_stream);
                afb_bytecode_write(bc, config);
                fputc(']', config->err_stream);
                break;
            case INS_PROGRAM:   afb_bytecode_write(bc, config); break;
            case INS_LEFT:      fputc('<', config->err_stream); break;
            case INS_RIGHT:     fputc('>', config->err_stream); break;
            case INS_INC:       fputc('+', config->err_stream); break;
            case INS_DEC:       fputc('-', config->err_stream); break;
            case INS_WRITE:     fputc('.', config->err_stream); break;
            case INS_READ:      fputc(',', config->err_stream); break;
            case INS_UP:        fputc('^', config->err_stream); break;
            case INS_DOWN:      fputc('v', config->err_stream); break;
            case INS_EXIT:      fputc('x', config->err_stream); break;
        }   
    }   
}
