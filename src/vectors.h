#include <cvector/vec_decl.h>

struct afb_bytecode;

DECLARE_VECTOR(afb_bcvec, struct afb_bytecode)
DECLARE_VECTOR(sizevec, size_t)
DECLARE_VECTOR(sizevecvec, sizevec)
