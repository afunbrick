struct afb_bf_machine {
    struct sizevec mem;
    size_t i;
    size_t mask;
};

void afb_bf_machine_init(struct afb_bf_machine* machine, struct afb_config* config);

int afb_bf_exec(struct afb_bf_machine* machine, struct afb_bytecode* bytecode, struct afb_config* config);

void afb_bf_machine_print_memory(struct afb_bf_machine* machine, struct afb_config* config);

void afb_bf_machine_free(struct afb_bf_machine* machine);
