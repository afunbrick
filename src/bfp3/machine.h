struct afb_bfp3_machine {
    struct sizevecvec mem;
    size_t i;
    size_t j;
    size_t mask;
};

void afb_bfp3_machine_init(struct afb_bfp3_machine* machine, struct afb_config* config);

int afb_bfp3_exec(struct afb_bfp3_machine* machine, struct afb_bytecode* bytecode, struct afb_config* config);

void afb_bfp3_machine_print_memory(struct afb_bfp3_machine* machine, struct afb_config* config);

void afb_bfp3_machine_free(struct afb_bfp3_machine* machine);
